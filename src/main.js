import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import 'jquery/src/jquery';
import "../node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js";
import "../node_modules/bootstrap-sass/assets/stylesheets/_bootstrap.scss";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
