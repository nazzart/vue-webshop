import Firebase from 'firebase';

// Initialize Firebase
var config = {
    apiKey: "AIzaSyA9NMrElHtiuN615joj1alS67md1GflLvY",
    authDomain: "webshop-a03b2.firebaseapp.com",
    databaseURL: "https://webshop-a03b2.firebaseio.com",
    projectId: "webshop-a03b2",
    storageBucket: "webshop-a03b2.appspot.com",
    messagingSenderId: "367691207741"
};
 
const firebaseApp = Firebase.initializeApp(config);
const db = firebaseApp.database();
export const dbShopRef = db.ref('shop');